package br.com.justoeu.example.log.exception;


import br.com.justoeu.example.log.domain.ws.ErrorVO;

import java.util.Set;
import java.util.TreeSet;

public class BusinessException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    private final Set<ErrorVO> errors;

    public BusinessException(final String message, final Set<ErrorVO> errors) {
        super(message);
        this.errors = errors;
    }

    public BusinessException(String message, String code, String field) {
        super(message);
        errors = new TreeSet<>();
        errors.add(new ErrorVO(field, code));
    }

    public BusinessException(Exception e, String code, String field) {
        super(e.getMessage());
        errors = new TreeSet<>();
        errors.add(new ErrorVO(field, code));
    }

    public void addError(ErrorVO error) {
        this.errors.add(error);
    }

    public Set<ErrorVO> getErrors() {
        return this.errors;
    }
}