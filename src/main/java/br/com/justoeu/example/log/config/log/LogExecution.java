package br.com.justoeu.example.log.config.log;

import com.google.common.base.Stopwatch;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.CodeSignature;
import org.slf4j.MDC;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.concurrent.TimeUnit.MILLISECONDS;

@Aspect
@Component
@Slf4j
public class LogExecution {

    @Around("@annotation(br.com.justoeu.example.log.config.log.LogExecutionTime)")
    public Object logExecutionTime(ProceedingJoinPoint joinPoint) throws Throwable {
        final Stopwatch sw = Stopwatch.createStarted();

        CodeSignature signature = (CodeSignature) joinPoint.getSignature();

        Object proceed = joinPoint.proceed();

        sw.stop();

        // Log message via Graylog UDP Logback Appender
        String[] paramNames = signature.getParameterNames();
        Object[] paramValues = joinPoint.getArgs();

        String params = Arrays.stream(paramValues).collect(Collectors.toList()).stream().map(Object::toString).collect(Collectors.joining(","));

        MDC.put("ClassName", String.valueOf(joinPoint.getSourceLocation().getWithinType().getName()));
        MDC.put("MethodExecuteCall", signature.getName() );
        MDC.put("ParamNames", String.join(",", paramNames) );
        MDC.put("MethodArgs", params);

        log.info(joinPoint.getSignature() + " - executed in {} ms", time(sw));

        MDC.clear();

        return proceed;
    }

    public static long time(final Stopwatch sw) {
        return sw.elapsed(MILLISECONDS);
    }


    public static BigDecimal time(final StopWatch sw) {
        double minutes = (double) (sw.getTotalTimeMillis() / 1000) / 60;
        return new BigDecimal(String.valueOf(minutes)).setScale(3, RoundingMode.HALF_EVEN);
    }

}