package br.com.justoeu.example.log.usecases;

import br.com.justoeu.example.log.domain.ws.ClientVO;

import java.util.List;

public interface ClientUC {

    List<ClientVO> getAllClients();

    List<ClientVO> getClientsByEmail(String email);
}
