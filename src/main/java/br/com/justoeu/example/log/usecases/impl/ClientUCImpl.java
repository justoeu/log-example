package br.com.justoeu.example.log.usecases.impl;

import br.com.justoeu.example.log.config.log.LogExecutionTime;
import br.com.justoeu.example.log.domain.entities.Client;
import br.com.justoeu.example.log.domain.ws.ClientVO;
import br.com.justoeu.example.log.usecases.ClientUC;
import com.github.javafaker.Faker;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

@Component
public class ClientUCImpl implements ClientUC {

    public static final int NUMBER_OF_CLIENTS = 100;
    public static final boolean PARALLEL      = false;
    public static final int NUMBER_OF_CLIENTS_FIXED = 2;

    @Override
    @LogExecutionTime
    public List<ClientVO> getAllClients() {
        return getClientVOS(NUMBER_OF_CLIENTS);
    }

    @Override
    @LogExecutionTime
    public List<ClientVO> getClientsByEmail(String email) {
        return getClientVOS(NUMBER_OF_CLIENTS_FIXED);
    }

    // @formatter:off
    private ClientVO createClientVO(Client client) {
        return client != null ? new ClientVO().builder().email(client.getEmail())
                .firstName(client.getFirstName())
                .lastName(client.getLastName())
                .id(client.getId())
                .build() : null;
    }

    private List<Client> createListClient(int numberOfClients){
        final Faker faker = new Faker();

        List<Client> clients = new ArrayList<>();

        for (int i=0 ; i < numberOfClients; i++){
            clients.add(new Client().builder()
                        .id(new Random().nextLong())
                        .email(faker.internet().emailAddress())
                        .firstName(faker.name().firstName())
                        .lastName(faker.name().lastName())
                    .build());
        }

        return clients;
    }

    private List<ClientVO> getClientVOS(int numberOfClients) {
        final Stream<Client> targetStream = StreamSupport.stream(createListClient(numberOfClients).spliterator(), PARALLEL);

        return targetStream.filter(Objects::nonNull)
                .map(this::createClientVO)
                .collect(Collectors.toList());
    }

    // @formatter:on
}
