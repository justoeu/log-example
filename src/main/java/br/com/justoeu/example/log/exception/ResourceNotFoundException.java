package br.com.justoeu.example.log.exception;

public class ResourceNotFoundException extends RuntimeException {

  private static final long serialVersionUID = 1L;

  private final String resource;

  public ResourceNotFoundException(String resource) {
    this.resource = resource;
  }

  public String getResource() {
    return resource;
  }

}