package br.com.justoeu.example.log.domain.ws;

import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ClientVO {

    private long id;
    private String firstName;
    private String lastName;
    private String email;

}