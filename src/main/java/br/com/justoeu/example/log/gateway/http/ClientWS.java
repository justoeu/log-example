package br.com.justoeu.example.log.gateway.http;

import br.com.justoeu.example.log.config.log.LogExecutionTime;
import br.com.justoeu.example.log.exception.ResourceNotFoundException;
import br.com.justoeu.example.log.domain.ws.ClientVO;
import br.com.justoeu.example.log.usecases.ClientUC;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@Slf4j
@Api(tags = "Clients")
@RequestMapping(URLMapping.ROOT_PATH)
public class ClientWS {

    @Autowired
    ClientUC action;

    @ApiOperation(value = "Get All Clients")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 404, message = "Not Found")
    })
    @RequestMapping(method = RequestMethod.GET, path = "/clients")
    @LogExecutionTime
    public ResponseEntity<List<ClientVO>> getAll() {

        final List<ClientVO> clients = action.getAllClients();

        if (clients.isEmpty()) {
            throw new ResourceNotFoundException("Not Clients inside BD!");
        }

        return new ResponseEntity<>(clients,HttpStatus.OK);
    }


    @ApiOperation(value = "Get Clients By Email")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 404, message = "Not Found")
    })
    @RequestMapping(method = RequestMethod.GET, path = "/client/email/{email:.+}")
    @LogExecutionTime
    public ResponseEntity<List<ClientVO>> getByEmail(@PathVariable("email") final String email) {

        final List<ClientVO> clients = action.getClientsByEmail(email);

        if (clients.isEmpty()) {
            throw new ResourceNotFoundException("not clients with this email");
        }

        return new ResponseEntity<>(clients,HttpStatus.OK);
    }

}
