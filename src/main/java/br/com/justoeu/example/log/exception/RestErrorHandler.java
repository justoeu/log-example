package br.com.justoeu.example.log.exception;

import br.com.justoeu.example.log.domain.ws.ResponseVO;
import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.convert.ConversionFailedException;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.HttpMediaTypeException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.mvc.support.DefaultHandlerExceptionResolver;

import javax.servlet.http.HttpServletRequest;

import static org.springframework.http.HttpStatus.*;

@ControllerAdvice
@Slf4j
public class RestErrorHandler extends DefaultHandlerExceptionResolver {

    @ExceptionHandler(HttpMessageNotReadableException.class)
    @ResponseBody
    @ResponseStatus(value = BAD_REQUEST)
    public ResponseVO handleMessageNotReadableException(final HttpMessageNotReadableException ex,
                                                        final HttpServletRequest request) {
        log.warn(ex.getMessage(), ex);
        return new ResponseVO(ex.getMessage(), BAD_REQUEST, request.getRequestURI());
    }

    @ExceptionHandler(ConversionFailedException.class)
    @ResponseBody
    @ResponseStatus(value = BAD_REQUEST)
    public ResponseVO handleConversionFailedException(final ConversionFailedException ex,
                                                    final HttpServletRequest request) {
        log.warn(ex.getMessage(), ex);
        return new ResponseVO(ex.getMessage(), BAD_REQUEST, request.getRequestURI());
    }

    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    @ResponseBody
    @ResponseStatus(value = BAD_REQUEST)
    public ResponseVO handleMethodArgumentTypeMismatchException(final ConversionFailedException ex,
                                                              final HttpServletRequest request) {
        log.warn(ex.getMessage(), ex);
        return new ResponseVO(ex.getMessage(), BAD_REQUEST, request.getRequestURI());
    }

    @ExceptionHandler(Throwable.class)
    @ResponseBody
    @ResponseStatus(value = INTERNAL_SERVER_ERROR)
    public ResponseVO handleGenericException(final Throwable ex, final HttpServletRequest request) {
        log.error(ex.getMessage(), ex);
        return new ResponseVO(ex.getMessage(), INTERNAL_SERVER_ERROR, request.getRequestURI());
    }

    @ExceptionHandler(BusinessException.class)
    @ResponseBody
    @ResponseStatus(value = UNPROCESSABLE_ENTITY)
    public ResponseVO businessException(final BusinessException ex, final HttpServletRequest request) {
        log.warn(ex.getMessage(), ex);
        return new ResponseVO(ex.getMessage(), UNPROCESSABLE_ENTITY, request.getRequestURI());
    }

    @ExceptionHandler(ResourceNotFoundException.class)
    @ResponseBody
    @ResponseStatus(value = NOT_FOUND)
    public ResponseVO resourceNotFoundException(final ResourceNotFoundException ex, final HttpServletRequest request) {
        log.info(ex.getMessage(), ex);
        return new ResponseVO(ex.getMessage(), NOT_FOUND, request.getRequestURI());
    }

    @ExceptionHandler(NotFoundException.class)
    @ResponseBody
    @ResponseStatus(value = NOT_FOUND)
    public ResponseVO notFoundException(final NotFoundException ex, final HttpServletRequest request) {
        log.info(ex.getMessage(), ex);
        return new ResponseVO(ex.getMessage(), NOT_FOUND, request.getRequestURI());
    }

    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    @ResponseStatus(METHOD_NOT_ALLOWED)
    @ResponseBody
    public ResponseVO methodNotSupportedException(final HttpRequestMethodNotSupportedException ex,
                                                final HttpServletRequest request) {
        log.warn(ex.getMessage(), ex);
        return new ResponseVO(ex.getMessage(), METHOD_NOT_ALLOWED, request.getRequestURI());
    }

    @ExceptionHandler(ServletRequestBindingException.class)
    @ResponseStatus(BAD_REQUEST)
    @ResponseBody
    public ResponseVO servletRequestBindingException(final ServletRequestBindingException ex,
                                                   final HttpServletRequest request) {
        log.warn(ex.getMessage(), ex);
        return new ResponseVO(ex.getMessage(), BAD_REQUEST, request.getRequestURI());
    }

    @ExceptionHandler(HttpMediaTypeException.class)
    @ResponseStatus(UNSUPPORTED_MEDIA_TYPE)
    @ResponseBody
    public ResponseVO unsupportedException(final HttpMediaTypeException ex, final HttpServletRequest request) {
        log.warn(ex.getMessage(), ex);
        return new ResponseVO(ex.getMessage(), UNSUPPORTED_MEDIA_TYPE, request.getRequestURI());
    }

    @ExceptionHandler(JsonProcessingException.class)
    @ResponseStatus(UNPROCESSABLE_ENTITY)
    @ResponseBody
    public ResponseVO jsonProcessingException(final JsonProcessingException ex, final HttpServletRequest request) {
        log.warn(ex.getMessage(), ex);
        return new ResponseVO(ex.getMessage(), UNPROCESSABLE_ENTITY, request.getRequestURI());
    }

}